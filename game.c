



#include <gb/gb.h>
#include <gb/drawing.h>
#include <gb/console.h>
#include <time.h>
#include <stdio.h>
//#include <rand.h>
//fixed seed;

#include "sprites/blank.c"
#include "background/mainMap.c"
#include "background/gameOverMap.c"
#include "background/winMap.c"

#include "background/stackerTiles.c"
#include "background/blankBlock.c"
#include "background/block.c"
#include "background/numbers/numbers.c"

//#include "background/assemblyMap.c"
//#include "background/grillAssemblyTiles.c"
typedef UINT8 bool;
#define TRUE       1
#define FALSE      0

#define NUMB_TILES 86
#define MAIN_SCREEN      0
#define WIN_SCREEN       1
#define GAME_OVER_SCREEN 2

#define STARTING_LAYER 0
#define FIRST_LAYER_Y 15
#define FIRST_BLOCK_X 1

#define TIMER_FACTOR 4

bool A_PRESSED      = FALSE;
bool B_PRESSED      = FALSE;
bool UP_PRESSED     = FALSE;
bool DOWN_PRESSED   = FALSE;
bool LEFT_PRESSED   = FALSE;
bool RIGHT_PRESSED  = FALSE;
bool SELECT_PRESSED = FALSE;
bool START_PRESSED  = FALSE;

UINT8 temp = 0;
UINT8 currentLevel = 0;
UINT8 currentLocation = MAIN_SCREEN; 
time_t currentTime = 0;
time_t lastTime = clock();
time_t timeSum = 0;
bool refresh = FALSE;
bool paused = FALSE; 
UINT16 index = 0;

UINT8 countdown = 20;

UINT8 currentBlockLocation[] = {0, 3, 6, 9, 12, 15, 18, 21, 24};
bool blockStatus[] = {
   0,0,1,1,1,1,1,0,0,
   0,0,0,1,1,1,0,0,0,   
   0,0,0,1,1,1,0,0,0, 
   0,0,0,1,1,0,0,0,0,   
   0,0,0,1,1,0,0,0,0,   
   0,0,0,1,0,0,0,0,0,   
   0,0,0,1,0,0,0,0,0,   
   0,0,0,1,0,0,0,0,0  
};
UINT8 blocksPerLayer[] = {5,3,3,2,2,1,1,1};
//dificulty
UINT8 countdownPerLayer[] =  {10,20,30,20,30,30,30,30,100};
UINT8 timerPerLayer[] =      {25,20,19,19,18,15, 12, 4,25};
UINT8 timeFactorPerLayer[] = {4, 10,15,20,25,30,30,40, 4};


UINT8 currentNumberOfBlocks = STARTING_LAYER;
bool directionRight = TRUE;
UINT8 timerIndex = TIMER_FACTOR;

void init();
void runEveryLoop();
void updateInput();
void updaterTimer(time_t inTicks);//input is how many ticks to trigger
void resetTimerFlag();
void displayNumber(UINT16 numb, UINT8 X, UINT8 Y, UINT8 sprite0, UINT8 sprite1, UINT8 sprite2);
void resetPressedKeys();


void pew();

void setLevelNumber(UINT8 numb);
void setTime(UINT8 time);
void placeDigit(UINT8 digit,UINT8 x, UINT8 y);

void resetCurrentLayer();
void resetBlockStatus();
void displayLayer(UINT8 numb);
void clearLayer(UINT8 numb);
void clearLastLayer();
void placeBlocks();
void drawBlank(UINT8 x, UINT8 y);
void drawBlock(UINT8 x, UINT8 y);
void updateBottom();//will update up to (exclusive) the current layer



 
void init(){
   


   set_bkg_data(0, NUMB_TILES , stackerTiles);
   set_bkg_tiles(0,0,20,36,mainMap);

   SHOW_BKG;

   resetBlockStatus();
   resetCurrentLayer();
   currentNumberOfBlocks = blocksPerLayer[STARTING_LAYER];
   currentLevel = STARTING_LAYER;
   currentLocation = MAIN_SCREEN; 
   countdown = countdownPerLayer[STARTING_LAYER];
}
void main(){
   init();
   drawCurrentLayer();
   updateBottom();

   while(1){
      runEveryLoop();

      if(paused){
         //printf("pause menu\n");
         if(B_PRESSED){
            B_PRESSED = FALSE;
            paused = FALSE;
            delay(100);
         }
      }
      else{//main game
         switch(currentLocation){
            case MAIN_SCREEN:
               runEveryLoop();
               if(A_PRESSED){
                  A_PRESSED = FALSE;
                  placeBlocks();
                  delay(250);
               }
               break;
            case GAME_OVER_SCREEN:
            case WIN_SCREEN:

               if(START_PRESSED){
                  START_PRESSED = FALSE;
                  init();
                  delay(300);
               }
              
               //printf("assembly\n");

               break; // end of assemly
            default:
               //printf("something Wrong with game contact developer");
               while(1){}
         }
      }


   }
}
void runEveryLoop(){


   updateInput();
   updaterTimer(timerPerLayer[currentLevel]);//1000);//one second of time in millis

   if(refresh){
      if(timerIndex == 0){
         timerIndex = timeFactorPerLayer[currentLevel];
         if(countdown <= 0){
            //placeBlocks();
            countdown = 20;

         }
         else{
            countdown--;
         }
      }
      else{
         timerIndex--;
      }


      if(directionRight){
         if(currentLevel >= 7){
            if(currentBlockLocation[blocksPerLayer[currentLevel]-1] < 18 ){
               moveBlocksRight();
            }
            else{
               directionRight = FALSE;
            }
         }
         else{

            if(currentBlockLocation[blocksPerLayer[currentLevel]-1] < 24 ){
               moveBlocksRight();
            }
            else{
               directionRight = FALSE;
            }
         }

      }
      else{
         if(currentLevel >=7){
            if(currentBlockLocation[0] > 6 ){
               moveBlocksLeft();
            }
            else{
               directionRight = TRUE;
            }
         }
         else{
            if(currentBlockLocation[0] > 0 ){
               moveBlocksLeft();
            }
            else{
               directionRight = TRUE;
            }
         }
      }

      if(currentLocation == MAIN_SCREEN){
         setTime(countdown);
         setLevelNumber(currentLevel+1);
         drawCurrentLayer();
      }
      resetTimerFlag();       
   }
}



void updateInput(){
   if(joypad() & J_START && joypad() & J_SELECT){  // If DOWN is pressed
      init();
      delay(200);
      //printf("start pressed\n");
   }
   if(joypad()==J_RIGHT){ // If RIGHT is pressed
      RIGHT_PRESSED = TRUE;
   } 
   if(joypad()==J_LEFT){  // If LEFT is pressed
      LEFT_PRESSED = TRUE;
   }
      
   if(joypad()==J_UP){  // If UP is pressed 
      UP_PRESSED = TRUE;
   }
   if(joypad()==J_DOWN){  // If DOWN is pressed
      DOWN_PRESSED = TRUE;
   }
   if(joypad()==J_A){  // If DOWN is pressed
      A_PRESSED = TRUE;
   }      
   if(joypad()==J_B){ // If DOWN is pressed
      B_PRESSED = TRUE;
   }
   if(joypad()==J_SELECT){  // If DOWN is pressed
      SELECT_PRESSED = TRUE;
      paused = TRUE;
      //printf("slect pressed\n");
   }
   if(joypad()==J_START){  // If DOWN is pressed
      START_PRESSED = TRUE;
      //printf("start pressed\n");
   }

   //waitpadup();
}
void updaterTimer(time_t inTicks){//input is how many millis to trigger
   currentTime = clock();
   timeSum += (currentTime - lastTime);
   lastTime = currentTime;
   if(timeSum*10 >= inTicks){
      refresh = TRUE;
   }
}
void resetTimerFlag(){
   refresh = FALSE;
   timeSum = 0;
}

void pew(){
   NR52_REG = 0x80;
   NR51_REG = 0x11;
   NR50_REG = 0x77;

   NR10_REG = 0x1E;
   NR11_REG = 0x10;
   NR12_REG = 0xF3;
   NR13_REG = 0x00;
   NR14_REG = 0x87;
}

void setLevelNumber(UINT8 numb){
   if(numb > 9){
      numb == 9;
   }
   placeDigit(numb,18,0);
}
void setTime(UINT8 time){
   if(time > 99){
      time == 99;
   }
   placeDigit(time/10,2,0);
   placeDigit(time%10,3,0);
}
void placeDigit(UINT8 digit,UINT8 x, UINT8 y){
   switch(digit){
      case 0:
         set_bkg_tiles(x,y,1,2,digit0);
         break;
      case 1:
         set_bkg_tiles(x,y,1,2,digit1);
         break;
      case 2:
         set_bkg_tiles(x,y,1,2,digit2);
         break;
      case 3:
         set_bkg_tiles(x,y,1,2,digit3);
         break;
      case 4:
         set_bkg_tiles(x,y,1,2,digit4);
         break;
      case 5:
         set_bkg_tiles(x,y,1,2,digit5);
         break;
      case 6:
         set_bkg_tiles(x,y,1,2,digit6);
         break;
      case 7:
         set_bkg_tiles(x,y,1,2,digit7);
         break;
      case 8:
         set_bkg_tiles(x,y,1,2,digit8);
         break;
      case 9:
         set_bkg_tiles(x,y,1,2,digit9);
         break;

   }
}






void drawBlock(UINT8 x, UINT8 y){
   set_bkg_tiles(x,y,2,2,block);
}
void drawBlank(UINT8 x, UINT8 y){
   set_bkg_tiles(x,y,2,2,blankBlock);
}
void resetBlockStatus(){
   UINT8 j;
   UINT8 k;
   for(j=0;j<8;j++){
      for(k=0;k<9;k++){
         blockStatus[(j*9)+k] = 0;
      }
   }
}
void resetCurrentLayer(){
   currentBlockLocation[0] = 0;
   currentBlockLocation[1] = 3;
   currentBlockLocation[2] = 6;
   currentBlockLocation[3] = 9;
   currentBlockLocation[4] = 12;
   currentBlockLocation[5] = 15;
   currentBlockLocation[6] = 18;
   currentBlockLocation[7] = 21;
   currentBlockLocation[8] = 24;
}

void displayLayer(UINT8 layerNumb){//0 indexed// will draw block, up to half block acurasy
   if (layerNumb <= 7){//will not handle last layer
      clearLayer(layerNumb);

   } 
}

void clearLayer(UINT8 numb){//will not clean last layer
   UINT8 y = FIRST_LAYER_Y-(numb*2);
   drawBlank(FIRST_BLOCK_X    , y);
   drawBlank(FIRST_BLOCK_X +2 , y);
   drawBlank(FIRST_BLOCK_X +4 , y);
   drawBlank(FIRST_BLOCK_X +6 , y);
   drawBlank(FIRST_BLOCK_X +8 , y);
   drawBlank(FIRST_BLOCK_X +10, y);
   drawBlank(FIRST_BLOCK_X +12, y);
   drawBlank(FIRST_BLOCK_X +14, y);
   drawBlank(FIRST_BLOCK_X +16, y);
}
void clearLastLayer(){//will clear last layer
   drawBlank(5   ,0);
   drawBlank(5+2 ,0);
   drawBlank(5+4 ,0);
   drawBlank(5+6 ,0);
   drawBlank(5+8 ,0);
}

void drawCurrentLayer(){//will draw depending on the position of each moving block
   UINT8 j;
   UINT8 x;
   UINT8 y;
   if(currentLevel >= 7){
      clearLastLayer();
   }
   else{
      clearLayer(currentLevel);
   }
   //display current later depending on where we at
   for(j=0;j< blocksPerLayer[currentLevel] ;j++){
      x = ((currentBlockLocation[j]/3)*2);
      y = FIRST_LAYER_Y-(currentLevel*2);
      if(currentLevel >=7){
         y--;
      }
      drawBlock(FIRST_BLOCK_X + x , y);
   }
}
void moveBlocksRight(){//make sure before calling this that they will not go over or under
   UINT8 x;
   for(x=0;x<9;x++){
      currentBlockLocation[x]++;
   }
}
void moveBlocksLeft(){
   UINT8 x;
   for(x=0;x<9;x++){
      currentBlockLocation[x]--;
   }
}
void updateBottom(){//will update up to (exclusive) the current layer
   UINT8 xIndex;
   UINT8 yIndex;

   UINT8 x;
   UINT8 y;
   for(yIndex=0;yIndex < currentLevel;yIndex++){
      if(currentLevel >= 7){
         clearLastLayer();
      }
      else{
         clearLayer(yIndex);
      }
      for(xIndex=0; xIndex < 9 ;xIndex++){
         if(blockStatus[(yIndex*9)+xIndex] == TRUE){
            x = ((currentBlockLocation[xIndex]/3)*2);
            y = FIRST_LAYER_Y-(yIndex*2);
            if(yIndex >=7){
               y--;
            }
            drawBlock(FIRST_BLOCK_X + x , y);
         }
      }
   }
}

void placeBlocks(){//will also make the ones falling blink
   UINT8 j;
   UINT8 x;
   bool atLEastOneLeft = FALSE;

   //display current later depending on where we at
   for(j=0;j< blocksPerLayer[currentLevel] ;j++){
      x = ((currentBlockLocation[j]/3));
      if(currentLevel == 0){
         blockStatus[x] = TRUE;
         atLEastOneLeft = TRUE;
      }
      else{
         if(blockStatus[((currentLevel-1)*9)+x] == TRUE){
            blockStatus[(currentLevel*9)+x] = TRUE;
            atLEastOneLeft = TRUE;
         }
      }
   } 
   resetCurrentLayer();
    currentLevel++;
   if(atLEastOneLeft == FALSE){
      currentLocation = GAME_OVER_SCREEN;
      set_bkg_tiles(0,0,20,36,gameOverMap);
   }
   else if(currentLevel>=8){
      currentLocation = WIN_SCREEN;
      set_bkg_tiles(0,0,20,36,winMap);
   }
   else{
      set_bkg_tiles(0,0,20,36,mainMap);
      updateBottom();  
   }
   if(currentLevel>=7){
      currentBlockLocation[0] = 6;//last leyer will start more to the right
   }
   countdown = countdownPerLayer[STARTING_LAYER];

}